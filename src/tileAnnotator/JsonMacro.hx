package tileAnnotator;

import tileAnnotator.Annotations.FieldType in FT;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

class JsonMacro {
	static var pos: Position;

	static var records : Map<String, Map<String, FT>>;
	static var enums : Map<String, Bool>;
	static var json : Dynamic;

	static final annot = "annotations";
	static final typeName = "TA_Tile";

	static var definitions : Array<TypeDefinition>;

	macro public static function addFields(name: String, path: String) : Array<Field> {
		buildTypes(path);
		var fields = Context.getBuildFields();

		var field : Field = {
			name: "tileSize",
			access: [APublic, AStatic],
			kind: FVar(TPath ({ name : "Int", pack : [] }), macro $v{json.tileSize}),
			pos: pos
		}
		fields.push(field);
 
		var tiles = buildExpr(json.tiles);
		var param = TPType (TPath ({ name : typeName, pack : [] }));
		var field : Field = {
			name: name,
			access: [APublic, AStatic],
			kind: FVar(TPath ({ name : "Array", pack : [], params : [param] }), tiles),
			pos: pos
		}
		fields.push(field);

		return fields;
	}

	macro public static function justTypes(path: String) : Array<Field> {
		buildTypes(path);
		var fields = Context.getBuildFields();
		return fields;
	}

	static var hasTypes = false;

	public static function buildTypes(path: String) {
		if (hasTypes)
			return;
		hasTypes = true;
		definitions = new Array();

		pos = Context.currentPos();
		records = new Map();
		enums = new Map();

		json = load(path);
		var fields = Context.getBuildFields();

		for (ename in Reflect.fields(json.types.enums)) {
			createEnum(ename, Reflect.getProperty(json.types.enums, ename));
		}

		for (rname in Reflect.fields(json.types.records)) {
			createRecord(rname, Reflect.getProperty(json.types.records, rname));
		}

		createRecord(annot, json.annotations);

		createTileRecord();

		var mod = Context.getLocalModule();
		haxe.macro.Context.defineModule(mod, definitions);
	}

	static function getIntExpr(vexpr : Dynamic) {
		return macro $v{Std.parseInt(vexpr)};
	}

	static function getFloatExpr(vexpr : Dynamic) {
		return macro $v{Std.parseFloat(vexpr)};
	}

	static function getBoolExpr(vexpr : Dynamic) {
		return macro $v{vexpr == "true" ? true : false};
	}

	static function getEnumExprFromString(n, v : Dynamic) : haxe.macro.Expr {
		var e =  Context.parse("TA_" + n +"." + v, pos);
		return e;
	}

	static function buildRecordExpr(n, t : Dynamic) {
		var isComplete = true;
		var rfields = records.get(n);

		var fields = new Array();

		for (f in rfields.keyValueIterator()) {
			var v = Reflect.field(t, f.key);

			if (null == v) {
				isComplete = false;
				break;
			}

			var expr =
				switch f.value {
					case FT.TInt: getIntExpr(v.value);
					case FT.TFloat: getFloatExpr(v.value);
					case FT.TBool: getBoolExpr(v.value);
					case FT.TEnum (n): getEnumExprFromString(n, v.value);
					case FT.TRecord (n): buildRecordExpr(n, v.value);
				}

			if (null == expr) {
				isComplete = false;
				break;
			}

			fields.push({field : f.key, expr : expr});
		}

		if(!isComplete) {
			return null;
		}

		return { pos : pos, expr : EObjectDecl (fields) };
	}

	static function buildExpr(tiles : Array<Dynamic>) {
		var values = new Array();

		for (t in tiles) {
			var r = t.annotations;

			var v = buildRecordExpr(annot, r);

			if (null == v) {
				continue;
			}

			var fields = new Array();

			fields.push({field : "annotations", expr : v});
			fields.push({field : "x", expr : macro $v{t.x}});
			fields.push({field : "y", expr : macro $v{t.y}});

			values.push({ pos : pos, expr : EObjectDecl (fields) });
		}
		
		return { pos : pos, expr : EArrayDecl (values) };
	}

	static function createRecord(n, fields) {
		var def : TypeDefinition = {
			name: "TA_" + n,
			pack: [],
			doc: "",
			kind: TDStructure,
			pos: pos,
			fields: new Array(),
		}

		var rfields = new Map();

		for (f in Reflect.fields(fields)) {
			var type = Reflect.getProperty(fields, f);

			var name = type;
		   	switch name {
				case "Int":
					rfields.set(f, FT.TInt);
			   	case "Float" :
					rfields.set(f, FT.TFloat);
				case "Bool":
					rfields.set(f, FT.TBool);
				default :
				   	name = "TA_" + name;
					if (enums.exists(type)) {
						rfields.set(f, FT.TEnum (type));
					} else {
						rfields.set(f, FT.TRecord (type));
					}
			}

			var field : Field = {
				name: f,
				access: [],
				kind: FVar(TPath ({ name : name, pack : [] }), null),
				pos: pos
			}

			def.fields.push(field);
		}

		records.set(n, rfields);

		definitions.push(def);
	}

	static function createTileRecord() {
		var def : TypeDefinition = {
			name: typeName,
			pack: [],
			doc: "",
			kind: TDStructure,
			pos: pos,
			fields: new Array(),
		}

		var field : Field = {
			name: "annotations",
			access: [],
			kind: FVar(TPath ({ name : "TA_"+annot, pack : [] }), null),
			pos: pos
		}
		def.fields.push(field);

		var field : Field = {
			name: "x",
			access: [],
			kind: FVar(TPath ({ name : "Int", pack : [] }), null),
			pos: pos
		}
		def.fields.push(field);

		var field : Field = {
			name: "y",
			access: [],
			kind: FVar(TPath ({ name : "Int", pack : [] }), null),
			pos: pos
		}
		def.fields.push(field);

		definitions.push(def);
	}

	static function createEnum(n, names) {
		var def : TypeDefinition = {
			name: "TA_" + n,
			pack: [],
			doc: "",
			kind: TDEnum,
			pos: pos,
			fields: names.map( function(n) : Field {
				return {
					name: n,
					pos: pos,
					kind: FVar(null, null),
				}
			}),
		}

		enums.set(n, true);

		definitions.push(def);
	}

	static function load(path : String) {
		return try {
			var json = haxe.Json.parse(sys.io.File.getContent(path));
			return json;
		} catch (e) {
			haxe.macro.Context.error('Failed to load json: $e', haxe.macro.Context.currentPos());
		}
	}
}
#end
