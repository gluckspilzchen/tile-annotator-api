package tileAnnotator;

import haxe.EnumTools;

enum Coord {
	XY (x : Int, y : Int);
}

enum FieldType {
	TInt;
	TFloat;
	TBool;
	TEnum(n : String);
	TRecord(n : String);
}

enum TypeDef {
	TDEnum(d : Array<String>);
	TDRecord(d : Map<String, FieldType>);
}

enum Field {
	FInt (i : Int);
	FFloat (f : Float);
	FBool (b : Bool);
	FEnum (n : String, v : String);
	FRecord (n : String, vs : Map<String, Field>);
}

typedef Annotation = {
	var type : String;
	var value : Dynamic;
}

typedef TileAnnotations = {
	var x : Int;
	var y : Int;
	var annotations : Map<String, Annotation>;
}

typedef ConstraintTypes<T> = {
	var enums : Map<String, Array<String>>;
	var records : Map<String, Map<String, T>>;
}

typedef SaveData =  {
	var tileSize : Int;
	var types : ConstraintTypes<String>;
	var tiles : Array<TileAnnotations>;
	var annotations : Map<String, String>;
}

typedef LoadData =  {
	var tileSize : Int;
	var types : ConstraintTypes<FieldType>;
	var annotations : Map<String, FieldType>;
	var tiles : Map<Coord, Map<String, Field>>;
}

class Utils {
#if (!js)
	static private final suffix = "_annot.json";
	static public function save(data : SaveData, filename : String) : Void {
		var data = dataToJson(data);
		sys.io.File.saveContent(filename + suffix, data);
	}

	static public function load(filename) : LoadData {
		var data = try { sys.io.File.getContent(filename + suffix); } catch (_) null;

		if (null == data)
			return null;

		return jsonToData(data);
	}
#end

	static function dataToJson(data : SaveData) : String {
		var data = haxe.Json.stringify(data, "\t");
		return data;
	}

	static function getInt (v : Dynamic) : Field {
		return FInt (Std.parseInt(v));
	}

	static function getFloat (v : Dynamic) : Field {
		return FFloat (Std.parseFloat(v));
	}

	static function getBool (v : Dynamic) : Field {
		return FBool (v == "true" ? true : false);
	}

	static function getEnum (t : String, n : Dynamic) : Field {
		return FEnum (t, n);
	}

	static function jsonToAnnotations (types: ConstraintTypes<FieldType>, ta : Dynamic) : Map<String, Field> {
		var as = new Map();
		for (f in Reflect.fields(ta)) {
			var a = Reflect.getProperty(ta, f);
			var na =
				if (a.type == "Int") {
					getInt(a.value);
				} else if (a.type == "Float") {
					getFloat(a.value);
				} else if (a.type == "Bool") {
					getBool(a.value);
				} else if (types.enums.exists(a.type)) {
					getEnum(a.type, a.value);
				} else {
					FRecord (a.type, jsonToAnnotations(types, a.value));
				};

			as.set(f, na);
		}

		return as;
	}

	static function jsonToData(data : String) : LoadData {
		var data = haxe.Json.parse(data);

		var tileSize = data.tileSize;
		var types = { enums : new Map(), records : new Map() };
		for (ename in Reflect.fields(data.types.enums)) {
			var constructors = Reflect.getProperty(data.types.enums, ename);
			types.enums.set(ename, constructors);
		}

		for (rname in Reflect.fields(data.types.records)) {
			var fieldData = Reflect.getProperty(data.types.records, rname);
			var fields = new Map();
			for (fname in Reflect.fields(fieldData)) {
				var typeString = Reflect.getProperty(fieldData, fname);
				var type =
					if (typeString == "Int") {
						TInt;
					} else if (typeString == "Float") {
						TFloat;
					} else if (typeString == "Bool") {
						TBool;
					} else if (types.enums.exists(typeString)) {
						TEnum (typeString);
					} else {
						TRecord (typeString);
					}
				fields.set(fname, type);
			}
			types.records.set(rname, fields);
		}

		var tiles = new Map();
		var annotations = new Map();

		for (fname in Reflect.fields(data.annotations)) {
			var typeString = Reflect.getProperty(data.annotations, fname);
			var type =
				if (typeString == "Int") {
					TInt;
				} else if (typeString == "Float") {
					TFloat;
				} else if (typeString == "Bool") {
					TBool;
				} else if (types.enums.exists(typeString)) {
					TEnum (typeString);
				} else {
					TRecord (typeString);
				}
			annotations.set(fname, type);
		}

		var dataTiles : Array<Dynamic> = data.tiles;
		for (ta in dataTiles) {
			var x = ta.x;
			var y = ta.y;
			var tannot = jsonToAnnotations(types, ta.annotations);
			tiles.set(XY (x, y), tannot);
		}

		return { types : types
			   , annotations : annotations
			   , tiles : tiles
			   , tileSize : tileSize
		};
	}
}
